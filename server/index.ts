import express from "express";
import { createConnection } from "typeorm";
import connectionOptions from "./ormConfig";
import { entities } from "./entity/entities";
import bodyParser from "body-parser";
import Route from "./routes";

const port = process.env.PORT_SERVER;

createConnection({ ...connectionOptions, entities }).then((connection) => {
  const app = express();
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  Route(app, connection);

  app.listen(port, () => {
    console.log(`app listening on port ${port}`);
  });
});
