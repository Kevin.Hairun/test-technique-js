import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from "typeorm";

export class tablesSetup1648734334773 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "user",
        columns: [
          {
            name: "id",
            type: "int",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "username",
            type: "varchar",
            length: "255",
            isNullable: false,
          },
          {
            name: "password",
            type: "varchar",
            length: "255",
            isNullable: false,
          },
        ],
      })
    );
    await queryRunner.createTable(
      new Table({
        name: "basket",
        columns: [
          {
            name: "id",
            type: "int",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "name",
            type: "varchar",
            length: "255",
            isNullable: false,
          },
          {
            name: "userId",
            type: "int",
            isNullable: false,
          },
        ],
      })
    );
    await queryRunner.createTable(
      new Table({
        name: "order",
        columns: [
          {
            name: "id",
            type: "int",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "id_basket",
            type: "int",
            isNullable: false,
          },
          {
            name: "creation_date",
            type: "timestamp",
            default: "NOW()",
          },
        ],
      })
    );
    await queryRunner.createTable(
      new Table({
        name: "product",
        columns: [
          {
            name: "id",
            type: "int",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "name",
            type: "varchar",
            length: "255",
            isNullable: false,
          },
          {
            name: "price",
            type: "int",
            default: 0,
          },
        ],
      })
    );
    await queryRunner.createTable(
      new Table({
        name: "product_basket",
        columns: [
          {
            name: "id",
            type: "int",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "id_basket",
            type: "int",
            isNullable: false,
          },
          {
            name: "id_product",
            type: "int",
            isNullable: false,
          },
          {
            name: "quantity",
            type: "int",
            default: 0,
          },
        ],
      })
    );
    await queryRunner.createForeignKey(
      "basket",
      new TableForeignKey({
        columnNames: ["userId"],
        referencedColumnNames: ["id"],
        referencedTableName: "user",
        // onDelete: "CASCADE",
        // onUpdate: "CASCADE",
      })
    );
    await queryRunner.createForeignKey(
      "order",
      new TableForeignKey({
        columnNames: ["id_basket"],
        referencedColumnNames: ["id"],
        referencedTableName: "basket",
        // onDelete: "CASCADE",
        // onUpdate: "CASCADE",
      })
    );
    await queryRunner.createForeignKey(
      "product_basket",
      new TableForeignKey({
        columnNames: ["id_basket"],
        referencedColumnNames: ["id"],
        referencedTableName: "basket",
        // onDelete: "CASCADE",
        // onUpdate: "CASCADE",
      })
    );
    await queryRunner.createForeignKey(
      "product_basket",
      new TableForeignKey({
        columnNames: ["id_product"],
        referencedColumnNames: ["id"],
        referencedTableName: "product",
        // onDelete: "CASCADE",
        // onUpdate: "CASCADE",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
