FROM node:12-alpine

WORKDIR /api

COPY package.json /api/package.json
COPY yarn.lock /api/yarn.lock

# Install packages
RUN yarn install

COPY . /api/

EXPOSE 4000

ENV POSTGRES_HOST postgres
ENV POSTGRES_PORT 5432

CMD yarn start
