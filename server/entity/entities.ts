import { Basket } from "./Basket";
import { Order } from "./Order";
import { ProductBasket } from "./Product_Basket";
import { Product } from "./Product";
import { User } from "./User";

export { Basket, Order, ProductBasket, Product, User };

export const entities = [Basket, Order, ProductBasket, Product, User];
