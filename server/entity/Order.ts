import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from "typeorm";

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ name: "id_basket", nullable: false })
  idBasket: string;

  @CreateDateColumn({
    name: "creation_date",
    type: "timestamp",
    nullable: false,
  })
  creationDate: Date;
}
