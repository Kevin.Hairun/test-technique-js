import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity("basket")
export class Basket {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ name: "userId", nullable: false })
  userId: number;
}
