import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity("product_basket")
export class ProductBasket {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "id_basket", nullable: false })
  idBasket: string;

  @Column({ name: "id_product", nullable: false })
  idProduct: string;

  @Column()
  quantity: number;
}
