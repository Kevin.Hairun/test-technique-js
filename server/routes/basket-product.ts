import { Application } from "express";
import { ProductBasket } from "../entity/Product_Basket";
import auth from "../middleware/auth";

const BasketProductRoute = (app: Application, connection: any) => {
  const ProductBasketRepository = connection.getRepository(ProductBasket);

  app.post("/basket/product", auth, async (req, res) => {
    //Add product on basket
    try {
      const productBasket = await ProductBasketRepository.create(req.body);
      const results = await ProductBasketRepository.save(productBasket);
      return res.send(results);
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });

  app.put("/basket/product", auth, async (req, res) => {
    //Update product on basket
    try {
      const productBasket = await ProductBasketRepository.findOne(req.body.id);
      if (productBasket) {
        await ProductBasketRepository.update(
          { id: req.body.id },
          { quantity: req.body.quantity }
        );
        return res.send({ ...productBasket, ...req.body });
      } else {
        res.send("Product not found in basket");
      }
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });

  app.delete("/basket/product", auth, async (req, res) => {
    //Remove product on basket
    try {
      const results = await ProductBasketRepository.delete({ id: req.body.id });
      return res.send(results);
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });
};

export default BasketProductRoute;
