import { Application } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { User } from "../entity/User";

const UserRoute = (app: Application, connection: any) => {
  const UserRepository = connection.getRepository(User);

  app.post("/register", async (req, res) => {
    try {
      const isUserExist = await UserRepository.findOne({
        where: { username: req.body.username },
      });
      if (!isUserExist) {
        const hashPassword = await bcrypt.hash(req.body.password, 10);
        const user = await UserRepository.create({
          username: req.body.username,
          password: hashPassword,
        });
        const results = await UserRepository.save(user);
        const token = await jwt.sign(
          { id: results.id, username: results.username },
          process.env.CONFIG_TOKEN || "",
          {
            expiresIn: "2h",
          }
        );
        return res.send(token);
      } else {
        res.status(400).send("User already exists");
      }
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });

  app.post("/login", async (req, res) => {
    //login
    try {
      const { username, password } = req.body;
      const user = await UserRepository.findOne({
        where: { username: username },
      });

      if (user && (await bcrypt.compare(password, user.password))) {
        // Create token
        const token = jwt.sign(
          { id: user.id, username: user.username },
          process.env.CONFIG_TOKEN || "",
          {
            expiresIn: "2h",
          }
        );
        // user
        res.send(token);
      } else {
        res.status(400).send("Invalid Credentials");
      }
    } catch (err) {
      console.log(err);
      res.send("Internal server error");
    }
  });
};

export default UserRoute;
