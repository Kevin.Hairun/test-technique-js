import { Application } from "express";
import { Basket } from "../entity/Basket";
import { Order } from "../entity/Order";
import auth from "../middleware/auth";

const BasketRoute = (app: Application, connection: any) => {
  const BasketRepository = connection.getRepository(Basket);
  const OrderRepository = connection.getRepository(Order);

  app.post("/basket", auth, async (req, res) => {
    //Create a basket
    try {
      const basket = await BasketRepository.create(req.body);
      const results = await BasketRepository.save(basket);
      return res.send(results);
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });

  app.post("/basket/validation", auth, async (req, res) => {
    //Validation basket and generation order
    try {
      const order = await OrderRepository.create(req.body);
      const results = await OrderRepository.save(order);
      return res.send(results);
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });
};

export default BasketRoute;
