import { Application } from "express";
import { Product } from "../entity/Product";
import auth from "../middleware/auth";

const ProductRoute = (app: Application, connection: any) => {
  const ProductRepository = connection.getRepository(Product);

  app.post("/product", auth, async (req, res) => {
    //Add a product
    try {
      const product = await ProductRepository.create(req.body);
      const results = await ProductRepository.save(product);
      return res.send(results);
    } catch (error) {
      console.log(error);
      res.send("Internal server error");
    }
  });
};
export default ProductRoute;
