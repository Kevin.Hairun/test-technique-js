import { Application } from "express";
import BasketRoute from "./basket";
import BasketProductRoute from "./basket-product";
import ProductRoute from "./product";
import UserRoute from "./user";

const Route = (app: Application, connection: any) => {
  UserRoute(app, connection);

  BasketRoute(app, connection);

  BasketProductRoute(app, connection);

  ProductRoute(app, connection);
};

export default Route;
