import { ConnectionOptions } from "typeorm";

const connectionOptions: ConnectionOptions = {
  name: "default",
  type: "postgres",
  url: "postgresql://root:root@db:5432/express",
  migrations: ["migrations/*.ts"],
  extra: {
    connectionLimit: process.env.POSTGRES_CONNECTION_LIMIT,
  },
  cache: false,
  cli: {
    migrationsDir: "migrations",
  },
  logging: ["error", "warn"],
};

export default connectionOptions;
